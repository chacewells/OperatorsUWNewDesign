package edu.uweo.javaintro.homework.operators;

public abstract class Operand
{
    private final int       sign_;
    private final String    operand_;
    
    public abstract double evaluate();
    
//    public Operand( String operand )
//    {
//        if ( operand.charAt( 0 ) == '-' )
//        {
//            sign_ = -1;
//            operand_ = operand.substring( 1 );
//        }
//        else
//        {
//            sign_ = 1;
//            operand_ = operand;
//        }
//    }
    
    public Operand( String operand )
    {
        int     sign    = 1;
        int     start   = 0;
        boolean done    = false;
        int     len     = operand.length();
        for ( int inx = 0 ; !done && inx < len ; ++inx )
        {
            char    test    = operand.charAt( inx );
            if ( test == '+' )
                ;
            else if ( test == '-' )
                sign *= -1;
            else
            {
                start = inx;
                done = true;
            }
        }
        sign_ = sign;
        operand_ = operand.substring( start );
    }
    
    public int getSign()
    {
        return sign_;
    }
    
    public String getOperand()
    {
        return operand_;
    }
    
    @Override
    public String toString()
    {
        String  result  = operand_;
        if ( sign_ < 0 )
            result = '-' + result;
        return result;
    }
}
