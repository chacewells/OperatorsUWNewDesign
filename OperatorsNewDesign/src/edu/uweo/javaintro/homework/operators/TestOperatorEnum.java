package edu.uweo.javaintro.homework.operators;

public class TestOperatorEnum
{
    private static TestCase[]   allTestCases_   =
    {
        new TestCase( "+", OperatorEnum.ADD ),
        new TestCase( "-", OperatorEnum.SUBTRACT ),
        new TestCase( "*", OperatorEnum.MULTIPLY ),
        new TestCase( "/", OperatorEnum.DIVIDE ),
        new TestCase( "&", null ),
    };
    
    public static void main( String[] args )
    {
        System.out.println( "start" );
        for ( TestCase testCase : allTestCases_ )
        {
            testCase.test();
            System.out.println( testCase );
        }
        System.out.println( "passed" );
    }
    
    private static class TestCase
    {
        private final String        symbol_;
        private final OperatorEnum  expectedEnum_;
        public TestCase( String symbol, OperatorEnum expectedEnum )
        {
            symbol_ = symbol;
            expectedEnum_ = expectedEnum;
        }
        
        public void test()
        {
            OperatorEnum    actualEnum  = OperatorEnum.getEnum( symbol_ );
            if ( actualEnum != expectedEnum_ )
            {
                String  msg =
                    "Expected: " + expectedEnum_
                    + ", Actual: " + actualEnum;
                throw new Error( msg );
            }
            if ( actualEnum != null )
            {
                String  actualSymbol    = actualEnum.getSymbol();
                if ( !symbol_.equals( actualSymbol ) )
                {
                    String  msg =
                        "Expected: " + symbol_
                        + ", Actual: " + actualSymbol;
                    throw new Error( msg );
                }
            }
        }
        
        public String toString()
        {
            String  result  = symbol_ + ": " + expectedEnum_;
            return result;
        }
    }
}
