package edu.uweo.javaintro.homework.operators;

public class TestExtraCredit
{
    private static TestCase[]   testCases_  =
                {
                    new TestCase( "-4", -4, -1 ),
                    new TestCase( "--4", 4, 1 ),
                    new TestCase( "---4", -4, -1 ),
                    new TestCase( "-++--++-+4", 4, 1 ),
                    new TestCase( "-++-++-++-++-4", -4, -1 ),
                };
    public static void main(String[] args)
    {
        System.out.println( "start" );
        for ( TestCase testCase : testCases_ )
            testCase.test();
        System.out.println( "passed" );
    }

    private static class TestCase
    {
        private static final double EPSILON = .00001;
        private final String    expression_;
        private final double    expectedValue_;
        private final int       expectedSign_;
     
        public TestCase( String expression, double value, int sign )
        {
            expression_ = expression;
            expectedValue_ = value;
            expectedSign_ = sign;
        }
        
        public void test()
        {
            OperandNumeric  operand     = new OperandNumeric( expression_ );
            double          actualValue = operand.evaluate();
            int             actualSign  = operand.getSign();
            if ( !isEqual( actualValue ) || actualSign != expectedSign_ )
            {
                String  msg =
                    expression_
                    + ", " + expectedValue_
                    + " (" + actualValue
                    + ") " + expectedSign_
                    + " (" + actualSign
                    + ")";
                throw new Error( msg );
            }
        }
        
        private boolean isEqual( double actualValue )
        {
            boolean result  = 
                Math.abs( expectedValue_ - actualValue) < EPSILON;
            return result;
        }
    }
}
