package edu.uweo.javaintro.homework.operators;

import java.util.StringTokenizer;

public class TestBinaryOperators
{
    private static String[] testCases_ =
    {
        "3.1*4=12.4",
        "M3.1*4=M12.4",
        "M3.1*M4=12.4",
        "3.1+4.2=7.3",
        "M3.1+4.2=1.1",
        "3.1-4.2=M1.1",
        "M3.1-4.2=M7.3",
        "1.2/4=.3",
        "M1.2/4=M.3"
        
    };
    public static void main(String[] args)
    {
        System.out.println( "begin" );
        for ( String str : testCases_ )
        {
            TestCase    testCase    = new TestCase( str );
            System.out.println( testCase );
            testCase.test();
        }
        System.out.println( "passed" );
    }

    private static class TestCase
    {
        private static final double EPSILON = .00001;
        
        private final String    expression_;
        private final String    leftOperand_;
        private final String    rightOperand_;
        private final String    operator_;
        private final String    expectedValue_;
        
        // To keep the parsing simple, substitute 'M' for a leading
        // minus sign:
        //     Instead of: -2.5
        //     Use: M2.5
        // Do not include any white space in the test string.
        public TestCase( String expression )
        {
            final String    delimiters  = "+-*/=";
            StringTokenizer tizer       =
                new StringTokenizer( expression, delimiters, true );
            if ( tizer.countTokens() != 5 )
                throw new IllegalArgumentException( expression );
            expression_ = expression;
            leftOperand_ = tizer.nextToken().replace( 'M', '-' );
            operator_ = tizer.nextToken();
            rightOperand_ = tizer.nextToken().replace( 'M', '-' );
            tizer.nextToken(); // toss the =
            expectedValue_ = tizer.nextToken().replace( 'M', '-' );
        }
        
        public void test()
        {
            OperatorEnum    eOperator;
            switch ( operator_ )
            {
            case "+":
                eOperator = OperatorEnum.ADD;
                break;
            case "-":
                eOperator = OperatorEnum.SUBTRACT;
                break;
            case "*":
                eOperator = OperatorEnum.MULTIPLY;
                break;
            case "/":
                eOperator = OperatorEnum.DIVIDE;
                break;
            default:
                throw new IllegalArgumentException( expression_ );
            }
            
            Operand         oper1       = new OperandNumeric( leftOperand_ );
            Operand         oper2       = new OperandNumeric( rightOperand_ );
            OperatorBinary  operator    = eOperator.getOperator( oper1, oper2 );
            double          actual      = operator.evaluate();
            if ( !isEqual( actual ) )
            {
                String  err = expression_ + "; actual = " + actual;
                throw new Error( err );
            }
        }
        
        private boolean isEqual( double actual )
        {
            double  expected    = Double.parseDouble( expectedValue_ );
            boolean result      = Math.abs( expected - actual ) < EPSILON;
            return result;
        }
        
        @Override
        public String toString()
        {
            String  result  =
                leftOperand_
                + operator_
                + rightOperand_
                + '='
                + expectedValue_;
            return result;
        }
    }
}
