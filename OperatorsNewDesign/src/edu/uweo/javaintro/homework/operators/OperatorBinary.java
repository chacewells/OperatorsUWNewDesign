package edu.uweo.javaintro.homework.operators;

import java.util.function.BiFunction;

public class OperatorBinary extends Operator
{
    
    private final Operand       operandLeft_;
    private final Operand       operandRight_;
    private final OperatorEnum  operator_;
    
    public
    OperatorBinary( OperatorEnum operator, Operand left, Operand right )
    {
        super( operator.getPriority() );
        operandLeft_ = left;
        operandRight_ = right;
        operator_ = operator;
    }
    
    public double getLeftValue()
    {
        double  val = operandLeft_.evaluate();
        return val;
    }
    
    public double getRightValue()
    {
        double  val = operandRight_.evaluate();
        return val;
    }
    
    public double evaluate()
    {
        BiFunction<Operand, Operand, Double> 
            operation   = operator_.getOperation();
        double result = operation.apply( operandLeft_, operandRight_ );
        return result;
    }
    
    @Override
    public String toString()
    {
        String  result  = 
            operandLeft_.toString() 
            + operator_
            + operandRight_.toString();
        return result;
    }
}
