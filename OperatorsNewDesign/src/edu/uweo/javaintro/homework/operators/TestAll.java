package edu.uweo.javaintro.homework.operators;

public class TestAll
{
    public static void main(String[] args)
    {
        System.out.println( "===TEST ALL===" );
        System.out.println( "TEST ENUMS" );
        TestOperatorEnum.main( args );
        System.out.println( "TEST OPERAND" );
        TestOperand.main( args );
        System.out.println( "TEST BINARY OPERATORS" );
        TestBinaryOperators.main( args );
        System.out.println( "TEST EXTRA CREDIT" );
        TestExtraCredit.main( args );
        System.out.println( "===TEST ALL PASSED===" );
    }
}
