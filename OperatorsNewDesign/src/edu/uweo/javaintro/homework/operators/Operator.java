package edu.uweo.javaintro.homework.operators;

public abstract class Operator
{
    private final int priority_;
    
    public Operator( int priority )
    {
        priority_ = priority;
    }
    
    public abstract double evaluate();
    
    public int getPriority()
    {
        return priority_;
    }
}
