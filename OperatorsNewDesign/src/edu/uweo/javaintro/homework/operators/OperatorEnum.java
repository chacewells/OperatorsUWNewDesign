package edu.uweo.javaintro.homework.operators;

import java.util.HashMap;
import java.util.function.BiFunction;

public enum OperatorEnum
{
    ADD( 4, "+", (op1, op2) -> op1.evaluate() + op2.evaluate() ),
    SUBTRACT( 4, "-", (op1, op2) -> op1.evaluate() - op2.evaluate() ),
    MULTIPLY( 5, "*", (op1, op2) -> op1.evaluate() * op2.evaluate() ),
    DIVIDE( 5, "/", (op1, op2) -> op1.evaluate() / op2.evaluate() );
    
    private final int                                   priority_;
    private final String                                symbol_;
    private final BiFunction<Operand, Operand, Double>  operation_;
    
    private static HashMap<String, OperatorEnum>        symbolMap_;
    static
    {
        symbolMap_ = new HashMap<>();
        for ( OperatorEnum eOper : OperatorEnum.values() )
        {
            String  symbol  = eOper.getSymbol();
            symbolMap_.put( symbol, eOper );
        }
    }
    
    private
    OperatorEnum(
        int priority,
        String symbol,
        BiFunction<Operand, Operand, Double> operation
    )
    {
        priority_ = priority;
        symbol_ = symbol;
        operation_ = operation;
    }
    
    public OperatorBinary getOperator( Operand operand1, Operand operand2 )
    {
        OperatorBinary  operator = null;
        if ( operation_ != null )
            operator = new OperatorBinary( this, operand1, operand2 );
        return operator;
    }
    
    public BiFunction<Operand, Operand, Double> getOperation()
    {
        return operation_;
    }
    
    public double evaluation( Operand operand1, Operand operand2 )
    {
        OperatorBinary  operator    = getOperator( operand1, operand2 );
        double          result      = operator.evaluate();
        return result;
    }

    // If symbol doesn't map to an operation null is returned.
    public static OperatorEnum getEnum( String symbol )
    {
        OperatorEnum    opEnum  = symbolMap_.get( symbol );
        return opEnum;
    }
    
    public int getPriority()
    {
        return priority_;
    }
    
    public final String getSymbol()
    {
        return symbol_;
    }
}
