package edu.uweo.javaintro.homework.operators;

public class TestOperand
{
    private static final TestCase[] testCases_  =
    {
        new TestCase( "42", 42 ),
        new TestCase( "-98", -98 ),
        new TestCase( "98.5", 98.5 ),
        new TestCase( "-42.5", -42.5 ),
        new TestCase( "0x20", 32 ),
        new TestCase( "-0x20", -32 ),
        new TestCase( "021", 17 ),
        new TestCase( "-021", -17 ),
        new TestCase( "0b1101", 13 ),
        new TestCase( "-0b1101", -13 ),
        new TestCase( "0", 0 ),
        new TestCase( "-0", 0 )
    };
    
    public static void main(String[] args)
    {
        System.out.println( "start" );
        for ( TestCase test : testCases_ )
        {
            System.out.println( test );
            test.test();
        }
        System.out.println( "passed" );
    }
}

class TestCase
{
    private static final double EPSILON         = .000001;
    
    private final String        stringValue_;
    private final double        expectedValue_;
    
    public TestCase( String stringValue, double expectedValue )
    {
        stringValue_ = stringValue;
        expectedValue_ = expectedValue;
    }
    
    public void test()
    {
        OperandNumeric  operand     = new OperandNumeric( stringValue_ );
        double          actualValue = operand.evaluate();
        int             actualSign  = operand.getSign();
        
        if ( !testSign( actualSign ) )
        {
            String  msg = "Test sign failure: "
                            + stringValue_
                            + " ("
                            + actualValue
                            + ")";
              throw new Error( msg );
        }
        
        if ( !isEqual( actualValue ) )
        {
            String  msg = "Test value failure: "
                          + stringValue_
                          + " ("
                          + actualValue
                          + ")";
            throw new Error( msg );
        }
    }
    
    private boolean isEqual( double actualValue )
    {
        boolean result  = Math.abs( expectedValue_ - actualValue ) < EPSILON;
        return result;
    }
    
    private boolean testSign( int actualSign )
    {
        int     expectedSign    = stringValue_.charAt( 0 ) == '-' ? -1 : 1;
        boolean result          = actualSign == expectedSign;
        return result;
    }
    
    @Override
    public String toString()
    {
        String  result  = "TestCase: "
                          + stringValue_
                          + " --> "
                          + expectedValue_;
        return result;
    }
}
