package edu.uweo.javaintro.homework.operators;

public class OperandNumeric
    extends Operand
{
    public OperandNumeric( String operand )
    {
        super( operand );
    }

    @Override
    public double evaluate()
    {
        String  operand = getOperand().toUpperCase();
        double  result;
        
        if ( operand.startsWith( "0X" ) )
        {
            String  sNum    = operand.substring( 2 );
            result = Integer.parseInt( sNum, 16 );
        }
        else if ( operand.startsWith( "0B" ) )
        {
            String  sNum    = operand.substring( 2 );
            result = Integer.parseInt( sNum, 2 );
        }
        else if ( operand.startsWith( "0" ) && operand.length() > 1 )
        {
            String  sNum    = operand.substring( 1 );
            result = Integer.parseInt( sNum, 8 );
        }
        else
            result = Double.parseDouble( operand );
        
        result *= getSign();
        return result;
    }
}
